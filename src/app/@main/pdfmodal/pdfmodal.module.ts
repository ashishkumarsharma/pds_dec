import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PdfmodalComponent } from './pdfmodal.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [PdfmodalComponent],
  imports: [
    CommonModule, 
    RouterModule,
    FormsModule,   
  ],
  exports:[PdfmodalComponent]
})
export class PdfmodalModule { }
