import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExhibitionStallComponent } from './exhibition-stall.component';

const routes: Routes = [
  {path:'', component:ExhibitionStallComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionStallRoutingModule { }
