import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GameZoneComponent } from './game-zone.component';

const routes: Routes = [
  {path:'', component:GameZoneComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameZoneRoutingModule { }
