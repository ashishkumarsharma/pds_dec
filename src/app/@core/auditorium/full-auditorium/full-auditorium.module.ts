import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FullAuditoriumRoutingModule } from './full-auditorium-routing.module';
import { FullAuditoriumComponent } from './full-auditorium.component';
import { AudiMenuModule } from '../audi-menu/audi-menu.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AskQuestionModule } from 'src/app/@main/shared/ask-question/ask-question.module';
import { PollModule } from 'src/app/@main/shared/poll/poll.module';
import { QuizModule } from 'src/app/@main/shared/quiz/quiz.module';
import { GroupChatModule } from 'src/app/@main/shared/group-chat/group-chat.module';


@NgModule({
  declarations: [FullAuditoriumComponent],
  imports: [
    CommonModule,
    FullAuditoriumRoutingModule,
    AudiMenuModule,
    FormsModule,
    ReactiveFormsModule,
    AskQuestionModule,
    PollModule,
    GroupChatModule,
    QuizModule
  ]
})
export class FullAuditoriumModule { }
